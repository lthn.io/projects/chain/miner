# Base image:
FROM golang:latest
MAINTAINER Julien Andrieux <julien@pantomath.io>


WORKDIR ~/go
RUN go get -u github.com/asticode/go-astilectron-bundler/...
RUN go install github.com/asticode/go-astilectron-bundler/astilectron-bundler@latest
WORKDIR /home/lthn/miner/src
COPY . .
RUN make build-ci