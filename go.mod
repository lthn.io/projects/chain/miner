module gitlab.com/lthn.io/projects/chain/miner

go 1.16

require (
	github.com/akavel/rsrc v0.10.2 // indirect
	github.com/asticode/go-astichartjs v0.1.0
	github.com/asticode/go-astikit v0.21.1
	github.com/asticode/go-astilectron v0.24.0
	github.com/asticode/go-astilectron-bootstrap v0.4.13
	github.com/google/uuid v1.2.0
	github.com/mitchellh/go-ps v1.0.0
	github.com/sirupsen/logrus v1.8.1
)
